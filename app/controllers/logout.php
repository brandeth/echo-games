<?php

// DESTROY ALL SESSIONS
session_start();
session_unset();
session_destroy();

// Route user to the landing page
header("Location: ../views/index.php");
